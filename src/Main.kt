import java.awt.*
import java.lang.System.exit
import java.util.Random
import javax.imageio.ImageIO
import javax.swing.*

private val font = Font("Courier New", Font.PLAIN, 12)

fun main(args: Array<String>)
{
    val config = Config(args)

    if (config.help)
    {
        println(config.usage)
        exit(0)
    }

    print("${args.toList().joinToString(" ")}, ")

    val app = JFrame()
    app.ignoreRepaint = true
    app.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    app.setBounds(150, 150, config.width, config.height)

    val canvas = Canvas()
    canvas.ignoreRepaint = true

    if (config.buttons) {
        for (i in 0 until config.numButtons) {
            val button = JButton("button $i")
            button.setBounds(
                100 + ((i / 10) * 200), 25 + ((i % 10) * 75), 150, 50)
            app.add(button)
        }
    }

    app.add(canvas)

    app.isVisible = true
    canvas.createBufferStrategy(2)
    val bufferStrategy = canvas.bufferStrategy

    val bufferedImage = GraphicsEnvironment
        .getLocalGraphicsEnvironment()
        .defaultScreenDevice
        .defaultConfiguration
        .createCompatibleImage(config.width, config.height)

    val unscaledImage = ImageIO.read(
        Config::javaClass.javaClass.getResource(
            if (config.transparentImages)
            {
                "/image-transparent.png"
            }
            else if (config.opacity50pcImages)
            {
                "/image-all-pixels-50pc-opacity.png"
            }
            else if (config.opacity0pc100pcImages)
            {
                "/image-pixels-either-100pc-or-0pc.png"
            }
            else if (config.transparentAreaImages)
            {
                "/image-large-transparent-area.png"
            }
            else
            {
                "/image.png"
            }
        )
    )

    val image =
        if (config.transparentAreaImages)
        {
            unscaledImage.getScaledInstance(
                config.imageSize * 2, config.imageSize * 2, Image.SCALE_SMOOTH)
        }
        else
        {
            unscaledImage.getScaledInstance(
                config.imageSize, config.imageSize, Image.SCALE_SMOOTH)
        }

    val singleLargeImage = unscaledImage.getScaledInstance(
        1200, 800, Image.SCALE_SMOOTH)

    val compositeImages = Array<Image>(100) {
        unscaledImage.getScaledInstance(20, 20, Image.SCALE_SMOOTH)
    }

    val unscaledImageWidth = unscaledImage.getWidth(null)
    val unscaledImageHeight = unscaledImage.getHeight(null)

    val backgroundColor = Color.BLACK
    val rand = Random()

    var secs = 0
    var totalFrames = 0

    var fpsLastSecond = 0
    var framesThisSecond = 0
    var timeThisSecond: Long = 0
    var curTime: Long = System.currentTimeMillis()
    var lastTime: Long

    while (true)
    {
        lastTime = curTime
        curTime = System.currentTimeMillis()
        timeThisSecond += curTime - lastTime
        if (timeThisSecond > 1000)
        {
            secs += 1
            if (secs > 15) {
                break
            }
            if (secs > 5) {
                totalFrames += framesThisSecond
            }
            timeThisSecond -= 1000
            fpsLastSecond = framesThisSecond
            framesThisSecond = 0
        }
        ++framesThisSecond

        val g2d = bufferedImage.createGraphics()
        try
        {
            g2d.color = backgroundColor
            g2d.fillRect(0, 0, config.width, config.height)

            if (config.rectangles)
            {
                for (i in 0 until config.numRectangles) {
                    g2d.color = Color(
                        rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)
                    )
                    g2d.fillRect(
                        rand.nextInt(config.width / 2),
                        rand.nextInt(config.height / 2),
                        rand.nextInt(config.width / 2),
                        rand.nextInt(config.height / 2)
                    )
                }
            }

            if (config.images)
            {
                val xoffset: Int
                val yoffset: Int
                if (config.offscreenImages)
                {
                    xoffset = 10000
                    yoffset = 10000
                }
                else
                {
                    xoffset = 0
                    yoffset = 0
                }

                for (i in 1..config.numImages)
                {
                    if (config.unscaledImages)
                    {
                        val s = config.imageSize
                        val x1 = xoffset + rand.nextInt(config.width)
                        val y1 = yoffset + rand.nextInt(config.height)
                        val x2 = x1 + s
                        val y2 = y1 + s
                        g2d.drawImage(
                            unscaledImage,
                            x1, y1, x2, y2,
                            0, 0, unscaledImageWidth, unscaledImageHeight,
                            null
                        )
                    }
                    else
                    {
                        g2d.drawImage(
                            image,
                            xoffset + rand.nextInt(config.width),
                            yoffset + rand.nextInt(config.height),
                            null
                        )
                    }
                }
            }

            if (config.singleLargeImage)
            {
                g2d.drawImage(
                    singleLargeImage,
                    10,
                    10,
                    null
                )
            }

            if (config.compositeImage)
            {
                for (y in 0 until 40)
                {
                    for (x in 0 until 60)
                    {
                        g2d.drawImage(
                            compositeImages[(y*20 + x) % compositeImages.size],
                            10 + (20 * x),
                            10 + (20 * y),
                            null
                        )
                    }
                }
            }

            if (config.text)
            {
                for (i in 1..config.numTexts)
                {
                    g2d.font =
                        if (config.newFont)
                        {
                            Font("Courier New", Font.PLAIN, 12)
                        }
                        else
                        {
                            font
                        }

                    g2d.color = Color.GREEN
                    g2d.drawString("FPS: $fpsLastSecond", 20, 20 + i * 14)
                }
            }

            val graphics = bufferStrategy.drawGraphics
            try
            {
                graphics.drawImage(bufferedImage, 0, 0, null)
                if (!bufferStrategy.contentsLost())
                {
                    bufferStrategy.show()
                }
            }
            finally
            {
                graphics.dispose()
            }
        }
        finally
        {
            g2d.dispose()
        }

        Thread.yield()
    }

    println("${totalFrames / 10}")
    exit(0)
}

class Config(args: Array<String>)
{
    val help: Boolean = ("--help" in args)
    val width: Int
    val height: Int
    val rectangles: Boolean
    val numRectangles: Int
    val buttons: Boolean
    val numButtons: Int
    val images: Boolean
    val unscaledImages: Boolean
    val numImages: Int
    val imageSize: Int
    val text: Boolean
    val numTexts: Int
    val newFont: Boolean
    val offscreenImages: Boolean
    val singleLargeImage: Boolean
    val compositeImage: Boolean
    var transparentImages: Boolean
    var opacity50pcImages: Boolean
    var opacity0pc100pcImages: Boolean
    var transparentAreaImages: Boolean

    val usage: String = """
        Usage: kotlin MainKt
            [large]
            [rectangles20|rectangles2]
            [buttons20|buttons2]
            [images20|images2] [largeimages] [unscaledimages]
                               [offscreenimages] [transparentimages]
            [text20|text2] [newfont]
            [singlelargeimage]
            [compositeimage]

        Measure frames per second for Java2D gameloop-style canvas drawing.
    """.trimIndent()


    init
    {
        if ("large" in args)
        {
            width = 1600
            height = 900
        }
        else
        {
                width = 640
                height = 480
        }

        when
        {
            "rectangles20" in args ->
            {
                rectangles = true
                numRectangles = 20
            }
            "rectangles2" in args ->
            {
                rectangles = true
                numRectangles = 2
            }
            else ->
            {
                rectangles = false
                numRectangles = -1
            }
        }

        when
        {
            "buttons20" in args ->
            {
                buttons = true
                numButtons = 20
            }
            "buttons2" in args ->
            {
                buttons = true
                numButtons = 2
            }
            else ->
            {
                buttons = false
                numButtons = -1
            }
        }

        when
        {
            "images20" in args ->
            {
                images = true
                numImages = 20
            }
            "images2" in args ->
            {
                images = true
                numImages = 2
            }
            else ->
            {
                images = false
                numImages = -1
            }
        }

        imageSize = if ("largeimages" in args) 400 else 40
        unscaledImages = ("unscaledimages" in args)

        when
        {
            "text20" in args ->
            {
                text = true
                numTexts = 20
            }
            "text2" in args ->
            {
                text = true
                numTexts = 2
            }
            else ->
            {
                text = false
                numTexts = -1
            }
        }

        newFont = ("newFont" in args)

        offscreenImages = ("offscreenimages" in args)
        singleLargeImage = ("singlelargeimage" in args)
        compositeImage = ("compositeimage" in args)
        transparentImages = ("transparentimages" in args)
        opacity50pcImages = ("transparent50pcimages" in args)
        opacity0pc100pcImages = ("transparent0pc100pcimages" in args)
        transparentAreaImages = ("transparentareaimages" in args)
    }
}

